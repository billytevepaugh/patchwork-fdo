from __future__ import absolute_import
from .base import *  # noqa

import logging

SECRET_KEY = '00000000000000000000000000000000000000000000000000'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ':memory:',
    },
}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
ENABLE_XMLRPC = True

# make tests faster
DEBUG = False
PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.MD5PasswordHasher',
)
logging.disable(logging.CRITICAL)
